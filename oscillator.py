import math

class HarmonicSinusConfig:
    def __init__(self):
        self.sampling_rate_hz = 44100
        self.phase_steps_per_cycle = 4410000
        self.base_frequency_hz = 400
        self.amplitude = 0.3
        self.harmonics = [0.3, 0.0, 0.2, 0.0, 0.05]

class HarmonicSinus:
    def __init__(self,config):
        self.config = config
        self.phase = 0

    def set_base_frequency(self, value):
        self.config.base_frequency_hz = value

    def set_amplitude(self, value):
        self.config.amplitude = value

    def set_harmonics(self, value):
        self.config.harmonics = value

    def step(self):
        phase_steps_per_sample = \
            1.0 / (float(self.config.sampling_rate_hz) / float(self.config.base_frequency_hz)) * float(self.config.phase_steps_per_cycle)
        self.phase = int(self.phase + phase_steps_per_sample) % self.config.phase_steps_per_cycle

    def render_value(self):
        result = 0.0

        phases = []
        phases.append(self.phase)

        for h in range(len(self.config.harmonics)):
            phases.append(self.phase * h)

        amplitudes = []
        amplitudes.append(self.config.amplitude)

        for h in range(len(self.config.harmonics)):
            amplitudes.append(self.config.amplitude * self.config.harmonics[h])

        for h in range(len(phases)):
            result = result + \
                amplitudes[h] * math.sin( 2.0 * math.pi / float(self.config.phase_steps_per_cycle) * float(phases[h]) )

        return result
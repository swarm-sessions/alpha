import oscillator

class SessionRenderer:
    def __init__(self,\
        oscillator_config, \
        pid_config_frequency, \
        pid_config_amplitude, \
        pid_config_harmonics, \
        jumps_per_minute_frequency, \
        jumps_per_minute_amplitude, \
        jumps_per_minute_harmonics):

        self.oscillator_config = oscillator_config

        self.pid_config_frequency = pid_config_frequency
        self.pid_config_amplitude = pid_config_amplitude
        self.pid_config_harmonics = pid_config_harmonics

        self.jumps_per_minute_frequency = jumps_per_minute_frequency
        self.jumps_per_minute_amplitude = jumps_per_minute_amplitude
        self.jumps_per_minute_harmonics = jumps_per_minute_harmonics
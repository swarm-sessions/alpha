import json

class Config:
    def __init__(self, source):
        self.sampling_rate_hz = source["sampling_rate_hz"]
        self.duration_s = source["duration_s"]
        self.seed = source["seed"]
        self.base_frequency_hz = source["base_frequency_hz"]
        self.base_amp = source["base_amp"]
        self.harmonics = source["harmonics"]
        self.target_path = source["target_path"]

    def print(self):
        print(f"Sampling Rate => {self.sampling_rate_hz}")
        print(f"Duration (s) => {self.duration_s}")
        print(f"Seed => {self.seed}")
        print(f"Base Frequency (Hz) => {self.base_frequency_hz}")
        print(f"Base Amplitude => {self.base_amp}")
        print(f"Harmonics => {self.harmonics}")
        print(f"Target Path => {self.target_path}")

    @staticmethod
    def load(path: str):
        loaded_config = None
        with open(path, 'r') as f:
            loaded_config = json.load(f)
        return Config(loaded_config)
    
import sys

import config

# Load configuration
config_path = sys.argv[1]
print(f"Load configuration from {config_path}")
config = config.Config.load(config_path)
config.print()

# Render session
import numpy as np

class IntegerRingBuffer:
    def __init__(self, size):
        self._size = size
        self._index = 0
        self._buffer = np.zeros( \
            shape=[self._size, 1], dtype=np.uint64 \
        )

    def _increment_index(self):
        self._index = (self._index + 1) % self._size

    def insert(self, value):
        self._buffer[self._index] = value
        self._increment_index()

    def tail(self):
        return np.copy(self._buffer[self._index])

    def head(self):
        return np.copy(self._buffer[ (self._index-1) % self._size ])

    def process(self, value):
        result = None

        if(isinstance(value, np.ndarray)): # Process multiple values
            result = np.zeros(shape=[value.size, 1], dtype=np.uint64)

            for x in np.arange(value.size):
                result[x] = self.tail()
                self.insert(value[x])
        else: # Process single value
            result = self.tail()
            self.insert(value)
        
        return result

class IntegerAccumulator:
    def __init__(self, buffer):
        self._accumulator = np.uint64(0)
        self._buffer = buffer

    def insert(self, new_value):
        last_buffer_value = self._buffer.process(new_value)
        self._accumulator = self._accumulator - last_buffer_value + new_value

    def accumulator(self):
        return self._accumulator

    def process(self, value):
        result = None

        if(isinstance(value, np.ndarray)): # Multiple values
            value_length = value.size
            result = np.zeros(shape=[value_length, 1], dtype=np.uint64)
            for x in np.arange(value_length):
                self.insert(value[x])
                result[x] = self.accumulator()
        else: # Single value
            self.insert(value)
            result = self.accumulator()
        
        return result

class MultiLayerIntegerAccumulator:
    def __init__(self, layers, window_size):

        if( isinstance(layers, int) and (layers > -1) ):
            # Create child layer
            if layers == 0:
                self._child_layer_acc = None
            else:
                self._child_layer_acc = MultiLayerIntegerAccumulator( \
                    layers - 1, \
                    window_size \
                )

            self._this_layer_acc = IntegerAccumulator( \
                IntegerRingBuffer(window_size) \
            )
        else:
            raise AssertionError("layers must be a positive integer!")

    def accumulator(self):
        return self._this_layer_acc.accumulator()

    def process(self, value):
        
        # Routine to process a single value
        def process_single_value(new_value):
            if(self._child_layer_acc):
                child_layer_result = self._child_layer_acc.process(new_value)
                return self._this_layer_acc.process(child_layer_result)
            else:
                return self._this_layer_acc.process(new_value)

        # Decide if single value to process or multiple
        if(isinstance(value, np.ndarray)): 
            value_length = value.size
            result = np.zeros(shape=[value_length, 1], dtype=np.uint64)
            for x in np.arange(value_length):
                result[x] = process_single_value(value[x])
            return result
        else:
            return process_single_value(value)

class NumericalRangeConverter:
    def __init__(self, float_min_value: np.float32, float_max_value: np.float32):
        self._float_min_value = float_min_value
        self._float_max_value = float_max_value

        self._normalize_offset = -float_min_value
        self._normalize_scale = np.float32(1.0) / (float_max_value - float_min_value)

        self._denormalize_scale = float_max_value - float_min_value
        self._denormalize_offset = +float_min_value

    def normalize(self, value: np.float32) -> np.uint64:
        clipped_value = np.clip(value, self._float_min_value, self._float_max_value)
        return self._normalized_float_to_uint32( \
            (clipped_value + self._normalize_offset) * self._normalize_scale \
        )

    def denormalize(self, value: np.uint32) -> np.float32:
        result = self._uint32_to_normalized_float(value)
        result = result * self._denormalize_scale
        result = result + self._denormalize_offset
        return result

    def _normalized_float_to_uint32(self, value: np.float32) -> np.uint32:
        return np.uint32( \
            np.float64(value) * np.float64(np.iinfo(np.uint32).max) \
        )

    def _uint32_to_normalized_float(self, value: np.uint32) -> np.float32:
        return np.float32( \
            np.float64(value) / np.float64(np.iinfo(np.uint32).max) \
        )

class AverageFilter:
    def __init__(self, window_size:np.uint32, in_min_value:np.float32 , in_max_value:np.float32):
        pass

    def step(self, value:np.float32):
        pass

    def output(self):
        pass

if __name__ == '__main__':

    if False:
        print("Test IntegerRingBuffer")
        window_size = 32
        test_signal_length = 64
        test_signal = np.zeros( \
            shape=[test_signal_length, 1], dtype=np.uint64 \
        )
        test_signal[0] = 1
        irb = IntegerRingBuffer(window_size)
        response_signal = irb.process(test_signal)

        # Dump response signal
        for s in range(response_signal.size):
            print(f"SAMPLE {s} IN [ {float(response_signal[s])} ] ")

    if False:
        print("Test IntegerAccumulator")
        window_size = 32

        # Generate test signal
        test_signal_length = 64
        test_signal = np.zeros( \
            shape=[test_signal_length, 1], dtype=np.uint64 \
        )
        test_signal[0] = 1

        # Processs test signal
        ia = IntegerAccumulator(IntegerRingBuffer(window_size))
        response_signal = ia.process(test_signal)

        # Dump response signal
        for s in range(response_signal.size):
            print(f"SAMPLE {s} IN [ {float(response_signal[s])} ] ")

    if False:
        print("Test MultiLayerIntegerAccumulator")
        layers = 7
        window_size = 128

        # Generate test signal
        test_signal_size = window_size + (window_size - 1)*layers
        test_signal = np.zeros( \
            shape=[test_signal_size, 1], dtype=np.uint64 \
        )
        test_signal[0] = 1

        # Processs test signal
        mlia = MultiLayerIntegerAccumulator(layers, window_size)
        response_signal = mlia.process(test_signal)

        # Dump response signal
        for s in range(response_signal.size):
            print(f"SAMPLE {s} IN [ {float(response_signal[s])} ] ")

    if True:
        print("Test NumericalRangeConverter")
        fmin = -3.0
        fmax = +5.0
        nrc = NumericalRangeConverter(fmin, fmax)

        v1 = -3.0
        v1_norm = nrc.normalize(v1)
        v1_denorm = nrc.denormalize(v1_norm)
        v1_delta = np.abs(v1 - v1_denorm)
        print(f"v1 (delta) = {v1_delta}")


        v2 = +5.0
        v2_norm = nrc.normalize(v2)
        v2_denorm = nrc.denormalize(v2_norm)
        v2_delta = np.abs(v2 - v2_denorm)
        print(f"v2 (delta) = {v2_delta}")
class PidControllerConfig:
    def __init__(self):
        self.kp = 0.0
        self.ki = 0.0
        self.kd = 0.0
        self.dt = 0.1

        self.previous_error = 0.0
        self.integral = 0.0
        self.setpoint = 0
        self.measured_value = 0.0

class PidController:
    def __init__(self, config):
        self.config = config

    def set_setpoint(self, value):
        self.setpoint = value

    def evaluate(self):
        error = self.setpoint - self.measured_value
        proportional = error
        self.integral = self.integral + error * self.config.dt
        derivative = (error - self.previous_error) / self.config.dt
        self.measured_value = self.config.kp * proportional + self.config.ki * self.integral + self.config.kd * derivative
        self.previous_error = error